// *** Dropdown Menu
$('.dropdown-toggle').on('click', (event) => {
    event.stopPropagation();
    event.preventDefault();
    let _this = $(event.currentTarget);
    $('.dropdown-menu').removeClass('show');
    if (!_this.hasClass('show')) {
        $('.dropdown-toggle').removeClass('show');
        $(event.currentTarget).addClass('show');
        $(' + .dropdown-menu', event.currentTarget).addClass('show');
        let menu = $(' + .dropdown-menu', event.currentTarget);
        let parents = $(event.currentTarget).closest('.modal-body').length > 0 ? $(event.currentTarget).closest('.modal-body')[0].clientWidth + $(event.currentTarget).closest('.modal-body').offset().left : window.innerWidth;
        let pos = menu.offset().left + menu.outerWidth() - parents;
        if (pos > -8) {
            menu.css('transform', 'translate(' + (0 - pos - 8) + 'px)');
        }
        $('body').not(' + .dropdown-menu').one('click', () => {
            $('.dropdown-toggle').removeClass('show');
            $('.dropdown-menu').removeClass('show');
            $(' + .dropdown-menu', event.currentTarget).css('transform', '');
        });
    } else {
        $(event.currentTarget).removeClass('show');
        $(' + .dropdown-menu', event.currentTarget).css('transform', '');
    }
});

// *** Modal
let modalCost = 0;
// modal z: 1100; backdrop z: 1010;
$('.modal .modal-header .btn.btn-close').on('click', (event) => {
    let target = $(event.target).attr('data-close');
    switch (target) {
        case 'MessageModal':
            MessageModalHide();
            break;
        default:
            modalHide(target);
            break;
    }
});
function modalShow(modalID, position) {
    position = position ? '#' + position : 'body';
    // if (!$('#' + modalID).is(':visible')) {
    if (!($('#' + modalID).css('display') != 'none')) {
        let cost = modalCost++;
        $('.modal-backdrop[data-modal-target="' + modalID + '"]').remove();
        $(position).append('<div class="modal-backdrop fade" data-modal-target="' + modalID + '" style="z-index: ' + (1010 + cost * 100) + '"></div>');
        setTimeout(() => {
            $('.modal-backdrop[data-modal-target="' + modalID + '"]').addClass('show');
            setTimeout(() => { $('#' + modalID).addClass('show').css('zIndex', (1100 + cost * 100)); }, 0);
        }, 100);
    }
    if (position != 'body') {
        $('.modal-backdrop[data-modal-target="' + modalID + '"]').css('position', 'absolute');
    }
}
function modalHide(modalID) {
    if ($('#' + modalID).css('display') != 'none') {
        modalCost--;
        $('#' + modalID).removeClass('show');
        setTimeout(() => {
            if ($('#' + modalID).css('display') == 'none') {
                $('.modal-backdrop[data-modal-target="' + modalID + '"]').removeClass('show');
                setTimeout(() => {
                    if ($('#' + modalID).css('display') == 'none') {
                        $('.modal-backdrop[data-modal-target="' + modalID + '"]').remove();
                    }
                }, 100);
            }
        }, 100);
    }
}

// *** Overlay
function overlayShow(overlayID) {
    if (!($('#' + overlayID).css('display') != 'none')) {
        modalCost++;
        setTimeout(() => {
        $('#' + overlayID).addClass('active').css('zIndex', (1010 + modalCost * 100));
            setTimeout(() => { $('#' + overlayID).addClass('show'); }, 0);
        }, 0);
    }
}
function overlayHide(overlayID) {
    if ($('#' + overlayID).css('display') != 'none') {
        modalCost--;
        $('#' + overlayID).removeClass('show').css('zIndex', '');
        $('#' + overlayID).removeClass('active');
    }
}

// *** Parser
function now(arg) {
    if (!arg) {
        return new Date().toLocaleString(undefined, {hour12: false});
    } else if (arg === 'u') {
        return Math.round(new Date().getTime() / 1000.0);
    } else if (!isNaN(arg)) {
        return new Date(arg * 1000).toLocaleString(undefined, {hour12: false});
    } else {
        return '';
    }
}
function formatnumber(num, min, max) {
    if (min && min > num) num = min;
    if (max && max < num) num = max;
    return num;
}
function formatsize(num, unit) {
    if (unit && unit == 'KB') num = num * 1024;
    if (unit && unit == 'MB') num = num * 1024 * 1024;
    return size = (num > 1024
        ? (Math.floor(num / 1024) > 1024
            ? Math.floor(num / 1024 / 1024) > 1024
                ? Math.floor(num / 1024 / 1024 / 10.24) / 100 + 'GB'
                : Math.floor(num / 1024 / 10.24) / 100 + 'MB'
            : Math.floor(num / 10.24) / 100 + 'KB')
        : num + 'B');
}
function formattime(num, unit, plusCB) {
    num = parseInt(num, 10);
    if (isNaN(num)) { return '00'; }
    if (unit == 'h') {
        if (num > 23) { plusCB && plusCB() }
        num = num == '' ? '0' : num > 23 ? '0' : num < 0 ? '23' : num;
    } else if (unit == 'm' || unit == 's') {
        if (num > 59) { plusCB && plusCB() }
        num = num == '' ? '0' : num > 59 ? '0' : num < 0 ? '59' : num;
    }
    return ('0' + num).slice(-2);
}
function formatdate(data, plusNum) {
    data = data.split('/');
    if (isNaN(plusNum)) { plusNum = 0; }
    let nd = new Date(data[0], data[1] - 1, parseInt(data[2], 10) + parseInt(plusNum, 10));
    return nd.getFullYear() + '/' + ('0' + (nd.getMonth() + 1)).slice(-2) + '/' + ('0' + nd.getDate()).slice(-2);
}

// *** Cookie
function setCookie(cname, cvalue, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var expires = 'expires=' + exdate.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}
function getCookie(name) {
    var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)');
    return (arr = document.cookie.match(reg)) ? (arr[2]) : null;
}

// *** Check
function passwordStrongCheck(mString, mCheckobj) {
    let pwd = mString;
    let hasUpperChar = /[A-Z]+/.test(pwd);
    let hasLowChar = /[a-z]+/.test(pwd);
    let hasDigit = /[0-9]+/.test(pwd);
    let hasSymbol = /[^A-Za-z0-9]/.test(pwd);
    // let Special = /[^a-zA-Z0-9@^&\/\.\-_~#\+="':;\(\)\\%\*!<>\?,\ ]/;
    let level = 0;
    let checked = false;

    if (pwd.length >=12 && hasSymbol && (hasUpperChar + hasLowChar + hasDigit) >= 2) {
        level = 2;
        checked = true;
    } else if (pwd.length >= 8 && (hasSymbol + hasUpperChar + hasLowChar + hasDigit) >= 3) {
        level = 1;
        checked = true;
    }
    // if (Special.test(pwd)) {
    //     checked = false;
    // }

    if (mCheckobj) {
        // if (Special.test(pwd)) {
        //     mCheckobj.removeClass('text-primary text-success').addClass('text-danger');
        //     mCheckobj.text(g_langFile['LOGIN_SECURITY_INVALID']);
        // } else
        if (level === 2) {
            mCheckobj.removeClass('text-red text-blue').addClass('text-green');
            mCheckobj.text(g_langFile['MSG_SECURITY_STRONG']);
        } else if(level === 1) {
            mCheckobj.removeClass('text-red text-green').addClass('text-blue');
            mCheckobj.text(g_langFile['MSG_SECURITY_NORMAL']);
        } else {
            mCheckobj.removeClass('text-blue text-green').addClass('text-red');
            mCheckobj.text(g_langFile['MSG_SECURITY_WEAK']);
        }
    }
    return checked;
}


